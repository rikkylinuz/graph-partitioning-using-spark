<h3>Graph Partitioning using Spark</h3>

Implemented a spark program using scala to partition a directed graph into k clusters in a breadth-first search fashion with multiple iterations.
