import org.apache.spark.SparkContext
import org.apache.spark.SparkConf

object Partition {

  val depth = 6

  def main ( args: Array[ String ] ) {
    val conf = new SparkConf().setAppName("Partition");
    val sc = new SparkContext(conf);
    var count = 0;
    var graph = sc.textFile(args(0)).map(l => {val s = l.split(",")
      count+=1
      if(count<=5)
	      (s(0).toLong,s(0).toLong,s.tail.map(_.toLong).toList)
      else
        (s(0).toLong,-1.toLong,s.tail.map(_.toLong).toList)
    }).collect
    var graphRDD = sc.parallelize(graph)
    for(i <- 1 to depth)
      graphRDD = graphRDD.flatMap(line=>{ val outputSeq = for(i <- 0 until line._3.size) yield{ (line._3(i),line._2)}; outputSeq:+(line._1,line._2)})
        .reduceByKey(_ max _)
        .join( graphRDD.map(node => {(node._1,(node._2,node._3))}) )
        .map{n => {
          if(n._2._2._1 != -1.toLong)
            (n._1,n._2._2._1,n._2._2._2);
          else
            (n._1,n._2._1,n._2._2._2);
        }}
//    graphRDD.collect().foreach(println);
    val results = graphRDD.map(l => (l._2)).countByValue()
    results.foreach(println)
  }
}
